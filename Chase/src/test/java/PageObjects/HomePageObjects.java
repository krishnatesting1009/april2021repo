package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePageObjects {

private static WebElement element = null;

public static WebElement txt_ticketNumber(WebDriver driver)
{
	element=driver.findElement(By.name("requests[0].ticketNumber"));
	return element;
}

public static WebElement txt_passengerLastName(WebDriver driver)
{
	element=driver.findElement(By.name("requests[0].passengerLastName"));
	return element;
}

public static WebElement btn_validateTicket(WebDriver driver)
{
	element=driver.findElement(By.name("image"));
	return element;
}

public static WebElement btn_submitRequest(WebDriver driver)
{
	element=driver.findElement(By.id("requestSubmitButton"));
	return element;
}

public static WebElement btn_passengerContactInformation(WebDriver driver)
{
	element=driver.findElement(By.xpath("//*[@id=\"selects\"]/div[10]/div/h3"));
	return element;
}
public static WebElement btn_add(WebDriver driver)
{
	element=driver.findElement(By.name("add"));
	return element;
}
}