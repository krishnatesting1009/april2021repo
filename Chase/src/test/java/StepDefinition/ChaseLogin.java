package StepDefinition;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import Utils.CommonMethods;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ChaseLogin {
public static WebDriver driver;
WebElement element;

@Given("^User is on Home Page$")
public void user_is_on_Home_Page() throws Throwable {

	System.setProperty("webdriver.chrome.driver","/Users/shk/Downloads/Automation/Drivers/Chrome/chromedriver");
	
	ChromeDriver driver = new ChromeDriver(); //Opens CB browser
	
	//System.setProperty("webdriver.chrome.driver", "C:\\SELENIUM\\ChromeDriver\\chromedriver.exe" );
	//driver = new ChromeDriver();
    //Implicit wait
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
       
    //Data driven approach. Read data from excel sheet instead of hard coding in the script
    driver.get(CommonMethods.getURL("/Users/shk/eclipse-workspace/Old_eclipse/chase/TestData/TestData.xlsx", 1, 0, "LOGIN"));

    //Maximize screen
    driver.manage().window().maximize();
       
    //Capture screenshot of Application for each step
    CommonMethods.captureScreenShot(driver,"Step1_Refund Application_Page_Displayed","TC1_Screenshots");
    CommonMethods.writeresult("/Users/shk/eclipse-workspace/Old_eclipse/chase/TestData/TestData.xlsx", 1, 0, 0, "Step1");
    CommonMethods.writeresultsamerow("/Users/shk/eclipse-workspace/Old_eclipse/chase/TestData/TestData.xlsx", 1, 1, 0, "Login_Page_Displayed");
    CommonMethods.writeresultsamerow("/Users/shk/eclipse-workspace/Old_eclipse/chase/TestData/TestData.xlsx", 1, 2, 0, "PASS");
}

@When("^User Navigate to LogIn Page$")
public void user_Navigate_to_LogIn_Page() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}

@When("^User enters UserName and Password$")
public void user_enters_UserName_and_Password() throws Throwable {
    // Write code here that turns the phrase above into concrete actions

}

@Then("^Message displayed Login Successfully$")
public void message_displayed_Login_Successfully() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    
}


}

