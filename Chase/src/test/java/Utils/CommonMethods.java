package Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.sql.Connection;  
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CommonMethods {

public static String dbActionTaken=null;
public static String dbAccountNo = null;
public static String dbASAPllocation = null;
public static String dbDebitAccount = null;
public static String dbCreditAccount = null;

static XSSFWorkbook wb; // This is POI library reference to a work book
static XSSFSheet sheet; // This is POI library reference to a sheet

public static String getURL(String filePath, int Row, int Column, String Sheet) throws IOException{
	File src = new File(filePath);
	FileInputStream fis = new FileInputStream(src);
	wb = new XSSFWorkbook(fis);
	sheet=wb.getSheet(Sheet);
	String data = sheet.getRow(Row).getCell(Column).getStringCellValue();
	return data;
}

public static void writeresult(String filePath, int Row, int Column, int Sheet, String Value) throws IOException{
	File src = new File(filePath);
	FileInputStream fis = new FileInputStream(src);
	wb = new XSSFWorkbook(fis);
	
	sheet=wb.getSheetAt(Sheet);
	//if(Sheet!=0)
	sheet.createRow(Row);
	sheet.getRow(Row).createCell(Column).setCellValue(Value);
	
	FileOutputStream fout=new FileOutputStream(new File(filePath));
	wb.write(fout);
}

public static void writeresultsamerow(String filePath, int Row, int Column, int Sheet, String Value) throws IOException{
	File src = new File(filePath);
	FileInputStream fis = new FileInputStream(src);
	wb = new XSSFWorkbook(fis);
	
	sheet=wb.getSheetAt(Sheet);
	//if(Sheet!=0)
	//sheet.createRow(Row);
	sheet.getRow(Row).createCell(Column).setCellValue(Value);
	
	FileOutputStream fout=new FileOutputStream(new File(filePath));
	wb.write(fout);
}

public static String getData(String filepath, int sheetnumber, int rownumber, int colnum) throws IOException {
	File src = new File(filepath);
	FileInputStream fis = new FileInputStream(src);
	wb = new XSSFWorkbook(fis);
	sheet = wb.getSheetAt(sheetnumber);
	String data = sheet.getRow(rownumber).getCell(colnum)
	.getStringCellValue();
	return data;
}


public int getRowCount(int sheetIndex) {
	int rowcount = wb.getSheetAt(sheetIndex).getLastRowNum();
	return rowcount;
}


public static void captureScreenShot(WebDriver ldriver, String value, String folder) throws IOException{
	// Take screenshot and store as a file format            
	File src=((TakesScreenshot)ldriver).getScreenshotAs(OutputType.FILE);  
	  try {
	// now copy the  screenshot to desired location using copyFile method
	FileUtils.copyFile(src, new File("/Users/shk/eclipse-workspace/Old_eclipse/chase/TC1_Screenshots"+".png"));                              }
	catch (IOException e)
	{
	 System.out.println(e.getMessage());
}
}

public static void selectQuery() throws SQLException, ClassNotFoundException {
	String dbURL = "jdbc:db2://10.52.160.28:5040/AWAMVSDB2T";
	String username = "xxx";
	String password = "xxx";
    
   //Load DB2 JDBC Driver
   Class.forName("com.ibm.db2.jcc.DB2Driver");
   
   //Creating connection to the database
   Connection con = DriverManager.getConnection(dbURL,username,password);
   
   //Creating statement object
   Statement st = con.createStatement();
   String selectquery = "select * from CCAD0001.CCAT0130_CCHISTORY where CCH_TICKET_NO = '0010275639142' ORDER BY CCH_ACTION_TIMESTAMP DESC FETCH FIRST 1 ROWS ONLY ";
   
   //Executing the SQL Query and store the results in ResultSet
   ResultSet rs = st.executeQuery(selectquery);
   rs.next();  
   dbActionTaken = rs.getString("CCH_ACTION_TAKEN");
   dbAccountNo = rs.getString("CCH_ACCOUNT_NO_MASKED");
   dbASAPllocation = rs.getString("CCH_SAP_ALLOCATION");
   dbDebitAccount = rs.getString("CCH_DEBIT_ACCOUNT");
   dbCreditAccount = rs.getString("CCH_CREDIT_ACCOUNT");    

   //Closing DB Connection
   con.close();
}

public static String getData1(String filepath, int sheetnumber, int rownumber, int colnum) throws IOException {
File src = new File(filepath);
FileInputStream fis = new FileInputStream(src);
wb = new XSSFWorkbook(fis);
sheet = wb.getSheetAt(sheetnumber);
    //Find number of rows in excel file
    int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
    //Create a loop over all the rows of excel file to read it
    for (int i = 0; i < rowCount+1; i++) {
        Row row = sheet.getRow(i);
        //Create a loop to print cell values in a row
        for (int j = 0; j < row.getLastCellNum(); j++) {
            //Print excel data in console
            System.out.print(row.getCell(j).getStringCellValue()+"|| ");
        }
        System.out.println();
      }
    return "";
}

}